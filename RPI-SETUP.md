# RPI setup

```bash
cat /etc/os-release
PRETTY_NAME="Raspbian GNU/Linux 11 (bullseye)"
```

## Install & update
- sudo apt update && sudo apt install -y
- sudo reboot
- sudo raspi-config (ssh)
- sudo apt-get install -y git build-essential automake autoconf libtool gettext libasound2-dev tcl tk libftgl2
- sudo apt purge libreoffice*
- sudo apt purge wolfram-engine
- sudo apt autoremove
- sudo shutdown -h now

## Setup
- sudo nano /boot/config.txt

```bash
dtoverlay=disable-bt
[pi4]
arm_boost=1
```

- sudo nano /etc/security/limits.conf
```
@audio   -  rtprio     98
@audio   -  memlock    unlimited
@audio   -  nice      -19
```

- sudo nano /etc/systemd/system/network-online.target.wants/networking.service (VOIR FICHIER)


## Pure Data
- git clone https://github.com/pure-data/pure-data
- git clone https://github.com/pure-data/pd-icon
- cd pure-data/
- ./autogen.sh 
- ./configure 
- make
- sudo make install

```bash
$ pd -version
Pd-0.54.0 ("test1") compiled 12:05:17 Apr  3 2023
```

- Open Pd
- Add lib preferences : add path when adding new libraries
- Add libs (if needed) : hcs cyclone zexy tof else iemlib iemguts pduino puremapping list-abs
- For Else, you may need to compile it for your architecture
- sudo apt install pd-comport
- Startup : -font-weight normal

## Faust PD
- echo 'deb http://download.opensuse.org/repositories/home:/aggraef:/pd-faustgen2/Raspbian_11/ /' | sudo tee /etc/apt/sources.list.d/home:aggraef:pd-faustgen2.list
- curl -fsSL https://download.opensuse.org/repositories/home:aggraef:pd-faustgen2/Raspbian_11/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/home_aggraef_pd-faustgen2.gpg > /dev/null
- sudo apt update
- sudo apt install pd-faustgen2
- cp /usr/lib/pd/extra/faustgen2~ + comport > /home/pi/Documents/Pd/externals

Optional : 
- sudo apt install faust

## Startup
- mkdir ~/.config/autostart
- sudo nano ~/.config/autostart/start.desktop
```bash
[Desktop Entry]
Type=Application
Exec=bash /home/pi/start.sh
```
- sudo nano /home/pi/start.sh
- sudo chmod a+x ./start.sh : pd -open /<PATH>/main.pd -nogui & 
