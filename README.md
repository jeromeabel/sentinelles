# Sentinelle

## Description
"Sentinelle" is a sound and robotic art installation made by Jérôme Abel.

A robot-probe is placed in a dark place, a cellar. It scans its surroundings with two lasers. The reliefs of the old walls, their imperfections, mold, appear thanks to the perfectly straight lines of the lasers. Its movements are synchronized with a sound program. From these very simple visual and sound choreographies are born an imaginary on its functioning and the origin of its presence. The spectator observes in a mirror a robot which observes and testifies. Does it communicate with other systems? Does it send information? Its energy is precious, like a heartbeat or a breath, it works in a segmented way with periods of silence. Likewise, his movements are slow and relentless.

## Media
- Video and images here : https://jeromeabel.net/workshop/sentinelle

## Hardware
- Rasberry Pi : See RPI-setup.md file for more details
- Arduino Uno
- Servos Shield 16 channels from Adafruit
- 2 * red line laser (5V, 5mW)
- 3 * standard servo motors
- Mechanics from gobilda
- Metal construction
- 3D print parts

## Software
- Compiled Pure Data 0.54
- Compiled Else library for this processor

Pure Data dependencies :
- https://github.com/agraef/pd-faustgen
- https://github.com/porres/pd-else

Faust objects from https://github.com/grame-cncm/faust/tree/master-dev/examples :
- examples/gameaudio/bubble
- examples/delayEcho/echo
- examples/gameaudio/rain
- examples/generator/sawtoothLab


## FR
Un robot-sonde est posé dans un endroit sombre, une cave. Il scanne son environnement avec deux lasers. Les reliefs des vieux murs, leurs imperfections, moisissures, apparaissent grâce aux lignes parfaitement droites des lasers. Ses mouvements sont synchronisées avec un programme sonore. De ces chorégraphies visuelles et sonores très simples naissent un imaginaire sur son fonctionnement et l'origine de sa présence. Le spectacteur observe en miroir un robot qui observe et témoigne. Communique-t-il avec d'autres systèmes ? Envoie-t-il des informations ? Son énergie est précieuse, comme un rythme cardiaque ou une respiration, il fonctionne de façon segmenté avec des plages de silence. De même ses mouvements sont lents et implacables.
